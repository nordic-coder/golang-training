package helper

import (
	"fmt"
)

func Test(str string, str2 string) (str3 string, length int) {
	str3 = fmt.Sprintf("%v - %v", str, str2)
	length = len(str3)
	return
}

// empty value, int, int32, int64
func FindMax(arr []int) (max int) {
	for _, v := range arr {
		if v > max {
			max = v
		}
	}
	return
}
