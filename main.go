package main

import (
	"fmt"

	"hello/helper"
)

func main() {
	str := "chuỗi ký tự "
	str2 := "2"

	var str4 string

	str4 = "chuỗi ký tự thứ 4"

	var str5 string = "chuỗi ký tự thứ 5"

	var str6 = "chuỗi ký tự thứ 6"

	var num1 int

	var num2 = 2

	num3 := 3

	fmt.Println(str4)

	fmt.Println(str5)
	fmt.Println(str6)

	fmt.Println(num1)
	fmt.Println(num2)
	fmt.Println(num3)

	str3, _ := helper.Test(str, str2)

	fmt.Println(str3)

	fmt.Println("-------------------------")

	arr := []int{54, 76, 98, 34, 76, 45, 34, 76, 76, 45}

	for _, v := range arr {
		fmt.Println(v)
	}

	fmt.Println(arr)

	fmt.Println(helper.FindMax(arr))

}
